#!/bin/bash
ROOT_UID=0
SUCCESS=0

if [ "$UID" -ne "$ROOT_UID" ]
then
  echo "Ejecute como root este script"
  echo "Format-> sudo ./name_file"
  exit $E_NOTROOT
fi  

file=$1

if [ "${file}X" = "X" ];
then
   echo "Debe pasar como parametro users.csv"
   echo "Format-> sudo ./name_file users.csv"
   exit 1
fi

eliminarUsuario(){

	userdel "${f1}"
	if [ $? -eq $SUCCESS ];
	then
		echo -e "\e[32m \t\t\tUsuario [${f1}] eliminado exitosamente\e[0m"
	else
		echo -e "\e[32m \t\t\tUsuario [${f1}] No se pudo eliminar\e[0m"
	fi
}
	sleep 3s
while IFS=: read f1 f2 f3 f4 f5 f6 f7
do
	eliminarUsuario "\${f1}"	
	sleep 2s
done < ${file}
sleep 4s
exit 0